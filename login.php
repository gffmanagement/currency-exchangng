<!DOCTYPE html>
<html lang="en">
<head>
    <title>Login page</title>
    <!-- css -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
 integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<!-- optional theme 1st is  https://bootswatch.com/slate/ -->
<link href="https://stackpath.bootstrapcdn.com/bootswatch/4.3.1/superhero/bootstrap.min.css" rel="stylesheet"
integrity="sha384-LS4/wo5Z/8SLpOLHs0IbuPAGOWTx30XSoZJ8o7WKH0UJhRpjXXTpODOjfVnNjeHu" crossorigin="anonymous">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
integrity="sha384-rHyoN1liRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPpp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="UTF-8">

</head>

<body>
<form action="log_proc.php" method="POST" class="container-fluid col-lg-9">
    <fieldset class="form-group">
        <a href="index.html" class="navbar-brand">Site for humans!</a> <br>
        <div><a href="reg.php">Needs to Register?</a></div>
        <label>
            <legend>Sign-in</legend>
            <input type='text' name='login' class="form-control"
                   id="exampleInputEmail1"
                   aria-describedby="emailHelp"
                   placeholder="Enter your e-mail"> <small id="emailHelp" class="form-text text-muted">We'll never share
                your email with anyone
                else.</small><br>

            <input type="password" name="password" class="form-control" id="exampleInputPassword"
                   placeholder="Enter your password"> <br>
            <button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
            <br>
        </label>
    </fieldset>
</form>
</body>
</html>
